<?php
session_start();
if (!isset($_SESSION["email"])) {
    header("Location: http://".$_SERVER['HTTP_HOST'].'/src/Users/View/UserLogin.php');
    exit();
}
