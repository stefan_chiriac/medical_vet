# Configurations


## CentOs server address:

```bash
gen-centos720300-all-dev.dmss.e4.c.emag.network
```

## Mysql

```mysql
Username = 'root';
Password = '';
Database Name = 'medical_vet';
Database Host = 'gen-centos720300-all-dev.dmss.e4.c.emag.network';
```