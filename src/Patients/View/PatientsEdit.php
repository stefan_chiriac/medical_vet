<html>
<head>
    <title>Edit Patient Data</title>
    <?php include("../../../src/Layout/header.php"); ?>
    <link rel="stylesheet" href="../../../resources/css/main.css">
    <link rel="stylesheet" href="../../../resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/css/jquery-ui.min.css">
</head>

<body>
<?php
include("../../../src/Patients/Service/PatientsEdit.php");
include("../../../config/auth_session.php");
?>

<br/><br/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div id='tablePanelBody' class='panel-body'>
                        <form name="update_" method="post"
                              action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>?id=<?php echo $id ?>">
                            <table class='table table-hover'>
                                <?php foreach ($patients as $patient) : ?>
                                    <tr>
                                        <td>ID #<?php echo $patient['id']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td><input type="text" class="form-control" name="name"
                                                   value=<?php echo $patient['name']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Race</td>
                                        <td><input type="text" class="form-control" name="race"
                                                   value=<?php echo $patient['race']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Species</td>
                                        <td><input type="text" class="form-control" name="species"
                                                   value=<?php echo $patient['species']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Birthday Date</td>
                                        <td><input type="date" class="form-control" name="birth_date"
                                                   value=<?php echo $patient['birth_date']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Sex</td>
                                        <td>
                                            <select name="sex" class="form-control">
                                                <option value="Male" <?php if ($patient['sex'] == 'Male') echo "selected=\"selected\"" ?>>
                                                    Male
                                                </option>
                                                <option value="Female" <?php if ($patient['sex'] == 'Female') echo "selected=\"selected\"" ?>>
                                                    Female
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Microchip</td>
                                        <td>
                                            <select name="microchip" class="form-control">
                                                <option value="true" <?php if ($patient['microchip'] == 'true') echo "selected=\"selected\"" ?>>
                                                    Yes
                                                </option>
                                                <option value="false" <?php if ($patient['microchip'] == 'false') echo "selected=\"selected\"" ?>>
                                                    No
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Health Card</td>
                                        <td>
                                            <select name="healthCard" class="form-control">
                                                <option value="true" <?php if ($patient['health_card'] === 'true') echo "selected=\"selected\"" ?>>
                                                    Yes
                                                </option>
                                                <option value="false" <?php if ($patient['health_card'] === 'false') echo "selected=\"selected\"" ?>>
                                                    No
                                                </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Owner</td>
                                        <td>
                                            <input type="text" class="form-control" name="owner"
                                                   value=<?php echo $patient['owner']; ?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><input type="hidden" value="<?php echo $patient['id']; ?>" name="id"></td>
                                        <td><input type="submit" name="submit" value="Submit" class="btn btn-primary">
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>