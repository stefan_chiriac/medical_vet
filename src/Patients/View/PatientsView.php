<!DOCTYPE>
<html lang="en">
<head>
    <title>Medical Vet - Patients List</title>
    <?php include("../../../src/Layout/header.php"); ?>
    <link rel="stylesheet" href="/resources/css/main.css">
</head>
<body>
<?php
include("../../../src/Patients/Service/PatientsView.php");
include("../../../config/auth_session.php");
?>

<div class="container">
    <br>
    <p class="text-center">Patients List</p>
    <hr>
</div>

<form class='main-form' name='view'>
    <?php foreach ($details as $view) : ?>
        <div class='form-group'>
            <label for='id'>Id</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['id']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='name'>Name</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['name']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='species'>Species</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['species']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='race'>Race</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['race']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='birth_date'>Birth Date</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['birth_date']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='sex'>Sex</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['sex']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='microchip'>Microchip</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['microchip']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='health_card'>Health Card</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['health_card']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='owner'>Owner</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['owner']; ?>" readonly='true'>
        </div>
        <div class='form-group'>
            <label for='created'>Created</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['created']; ?>" readonly='true'>
        </div>
    <?php endforeach; ?>
</form>
<br>
</body>
<?php include("../../../src/Layout/footer.php"); ?>
</html>