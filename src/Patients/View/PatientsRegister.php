<!DOCTYPE>
<html lang="en">
<head>
    <title>Medical Vet - Register</title>
    <?php include("../../../src/Layout/header.php"); ?>
    <link rel="stylesheet" href="/resources/css/main.css">
    <link rel="stylesheet" href="/resources/css/daterangepicker.min.css">

</head>
<body>

<?php
include("../../../config/auth_session.php");
include("../../../src/Patients/Service/PatientsRegister.php");
?>

<div class="container">
    <br>
    <p class="text-center">Patients Registration Form</p>
    <hr>
</div>

<form class="main-form" name="register" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <div class="form-group">
        <label for="name">Full Name *</label>
        <input type="text" name="name" id="name" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="species">Species *</label>
        <input type="text" name="species" id="species" class="form-control" required>
    </div>
    <div class="form-group">
        <label for="race">Race</label>
        <input type="text" name="race" id="race" class="form-control"" required>
    </div>
    <div class="form-group">
        <label for="birth_date">Date of Birth</label>
        <input type="date" name="birth_date" id="birth_date" class="form-control" required>
    </div>

    <div class="form-group">
        <label for="sex">Sex *</label>
        <input type="radio" name="sex" id="sex" class="form-group radio-inline" value="Male">M
        <input type="radio" name="sex" id="sex" class="form-group radio-inline" value="Female">F
    </div>
    <div class="form-group">
        <label for="microchip">Microchip *</label>
        <input type="radio" name="microchip" id="microchip" class="form-group radio-inline" value="true">Yes
        <input type="radio" name="microchip" id="microchip" class="form-group radio-inline" value="false">No
    </div>
    <div class="form-group">
        <label for="healthCard">Health Card *</label>
        <input type="radio" name="healthCard" id="healthCard" class="form-group radio-inline" value="true">Yes
        <input type="radio" name="healthCard" id="healthCard" class="form-group radio-inline" value="false">No
    </div>
    <div class="form-group">
        <label for="owner">Owner Name</label>
        <input type="text" name="owner" id="owner" class="form-control" required>
    </div>
    <div>
        <input type="submit" name="submit" value="Submit" class="btn btn-primary">
    </div>
</form>
</body>
</html>