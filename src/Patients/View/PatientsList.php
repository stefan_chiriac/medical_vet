<!DOCTYPE>
<html lang="en">
<head>
    <title>Medical Vet - Patients List</title>
    <?php include("../../../src/Layout/header.php"); ?>
</head>
<body>
<?php
include("../../../config/auth_session.php");
include("../../../src/Patients/Service/PatientsList.php");
include("../../../src/Layout/Messages.php");
?>
<script src="/resources/js/bootstrap.min.js"></script>

<div class="container">
    <div class="row">
        <div id="filter-panel" class="collapse filter-panel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-inline" role="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
                        <div class="form-group">
                            <label class="filter-col" for="sex">Sex:</label>
                            <select id="sex" class="form-control">
                                <option selected="selected" value="*" hidden></option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="filter-col" for="species">Species</label>
                            <select id="species" class="form-control">
                                <option value="" selected="selected"></option>
                                <option value="Dog">Dog</option>
                                <option value="Cat">Cat</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="filter-col" for="orderBy">Order by</label>
                            <select id="orderBy" class="form-control">
                                <option value="id" selected="selected">id</option>
                                <option value="name">name</option>
                                <option value="species">species</option>
                                <option value="race">race</option>
                                <option value="birth_date">birth date</option>
                                <option value="sex">sex</option>
                                <option value="microchip">microchip</option>
                                <option value="health_card">health card</option>
                                <option value="owner">owner</option>
                                <option value="created">created</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="filter-col" for="pref-asc-desc"></label>
                            <select id="pref-asc-desc" class="form-control">
                                <option value="asc" selected="selected">Ascending</option>
                                <option value="desc">Descending</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default filter-col">
                                <span class="fa fa-search"></span> Search
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#filter-panel">
            <span class="glyphicon glyphicon-cog"></span> Advanced Search
        </button>
    </div>
</div>

<br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div id="tablePanelBody" class="panel-body display">
                        <table class="table table-hover" id="patientsTable">
                            <thead>
                            <tr>
                                <th class="th" scope="col"> Id</th>
                                <th scope="col"> Name</th>
                                <th scope="col"> Species</th>
                                <th scope="col"> Race</th>
                                <th scope="col"> Birth Date</th>
                                <th scope="col"> Sex</th>
                                <th scope="col"> Microchip</th>
                                <th scope="col"> Health Card</th>
                                <th scope="col"> Owner</th>
                                <th scope="col"> Created</th>
                                <th scope="col"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($patients as $patient) : ?>
                                <tr>
                                    <th scope="row"><?php echo $patient['id']; ?></th>
                                    <td><?php echo $patient['name']; ?></td>
                                    <td><?php echo $patient['species']; ?></td>
                                    <td><?php echo $patient['race']; ?></td>
                                    <td><?php echo $patient['birth_date']; ?></td>
                                    <td><?php echo $patient['sex']; ?></td>
                                    <td><?php echo $patient['microchip'] ?></td>
                                    <td><?php echo $patient['health_card']; ?></td>
                                    <td><?php echo $patient['owner']; ?></td>
                                    <td><?php echo $patient['created']; ?></td>
                                    <td>
                                        <a href="PatientsEdit.php?id=<?php echo $patient['id'] ?>"><em
                                                    class="fa fa-edit" title="Edit User"></em></a>
                                        <a href="PatientsView.php?id=<?php echo $patient['id'] ?>"><em
                                                    class="fa fa-info-circle" title="View User"></em></a>
                                        <a href="PatientsDelete.php?id=<?php echo $patient['id'] ?>"><em
                                                    class="fa fa-trash" title="Delete User"></em></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#patientsTable').dataTable({
            "searching": false,
            "order": [],
            "filters": true,
            "sDom": '<"H"RlTfr>t<"F"ip>',
            "sScrollX": "",
            "bAutoWidth": false,
            "bJQueryUI": true,
            "ordering": false,
            "aoColumnDefs": [{
                "aTargets": [6,7]
                ,
                "mRender": function(data, type, row) {
                    return (row[6,7] == "true") ?
                        "Yes" : "No";
                }
            }]
        });
    });
</script>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">

    <!-- Add .modal-dialog-centered to .modal-dialog to vertically center the modal -->
    <div class="modal-dialog modal-dialog-centered" role="document">


        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo $patient['id'];  ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>