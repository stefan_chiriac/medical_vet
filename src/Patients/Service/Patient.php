<?php

require($_SERVER['DOCUMENT_ROOT'] . "/config/config.php");

class Patient
{
    protected $db;

    public function __construct()
    {
        $this->db = Config();
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function createPatient($name, $species, $race, $birthDate, $sex, $microchip, $healthCard, $owner)
    {
        $query = $this->db->prepare("INSERT INTO patients (name, species, race, birth_date, sex, microchip, health_card, owner) VALUES (:name, :species, :race, :birthDate, :sex, :microchip, :healthCard, :owner)");
        $query->bindParam("name", $name, PDO::PARAM_STR);
        $query->bindParam("species", $species, PDO::PARAM_STR);
        $query->bindParam("race", $race, PDO::PARAM_STR);
        $query->bindParam("birthDate", $birthDate, PDO::PARAM_STR);
        $query->bindParam("sex", $sex, PDO::PARAM_STR);
        $query->bindParam("microchip", $microchip, PDO::PARAM_STR);
        $query->bindParam("healthCard", $healthCard, PDO::PARAM_STR);
        $query->bindParam("owner", $owner, PDO::PARAM_STR);
        $query->execute();
        return $this->db->lastInsertId();
    }

    public function getAllPatients()
    {
        $query = $this->db->prepare("SELECT * FROM patients");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function deletePatient($patientId)
    {
        $query = $this->db->prepare("DELETE FROM patients WHERE id = :id");
        $query->bindParam("id", $patientId, PDO::PARAM_STR);
        $query->execute();
    }

    public function updatePatient($name, $species, $race, $birthDate, $sex, $microchip, $healthCard, $owner, $id)
    {
        $query = $this->db->prepare("UPDATE patients SET name = :name, species = :species, race = :race, birth_date = :birthDate, sex = :sex, microchip = :microchip, health_card = :healthCard, owner = :owner WHERE id = :id");
        $query->bindParam("id", $id, PDO::PARAM_STR);
        $query->bindParam("name", $name, PDO::PARAM_STR);
        $query->bindParam("species", $species, PDO::PARAM_STR);
        $query->bindParam("race", $race, PDO::PARAM_STR);
        $query->bindParam("birthDate", $birthDate, PDO::PARAM_STR);
        $query->bindParam("sex", $sex, PDO::PARAM_STR);
        $query->bindParam("microchip", $microchip, PDO::PARAM_STR);
        $query->bindParam("healthCard", $healthCard, PDO::PARAM_STR);
        $query->bindParam("owner", $owner, PDO::PARAM_STR);
        $query->execute();
    }

    public function getPatientDetails($patientId)
    {
        $query = $this->db->prepare("SELECT * FROM patients WHERE id = :id");
        $query->bindParam("id", $patientId, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

}
