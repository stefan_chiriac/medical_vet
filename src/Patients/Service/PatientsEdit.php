<?php

require($_SERVER['DOCUMENT_ROOT'] . "/src/Patients/Service/Patient.php");

$errorMessages = [];
$object = new Patient();

$id = $_GET['id'];

$patients = $object->getPatientDetails($id);

$name = $species = $race = $birthDate = $sex = $microchip = $healthCard = $owner = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $errorMessages[] = "Name is required";
    } else {
        $name = check_input($_POST["name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/", $name)) {
            $errorMessages[] = "Space and special characters not allowed but you can use underscore(_).";
        } else {
            $name = $name;
        }
    }

    if (empty($_POST["species"])) {
        $errorMessages[] = "Species is required";
    } else {
        $species = check_input($_POST["species"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/", $species)) {
            $errorMessages[] = "Space and special characters not allowed but you can use underscore(_).";
        } else {
            $species = $species;
        }
    }

    if (empty($_POST["race"])) {
        $errorMessages = "Race is required";
    } else {
        $race = check_input($_POST["race"]);
        // check if e-mail address is well-formed
        if (!preg_match("/^[a-zA-z0-9)]*$/", $race)) {
            $race = "Invalid race format";
        } else {
            $race = $race;
        }
    }

    if (empty($_POST["birth_date"])) {
        $errorMessages = "Birthday date is required";
    } else {
        $birthDate = check_input($_POST["birth_date"]);
    }

    if (empty($_POST["sex"])) {
        $errorMessages = "Sex is required";
    } else {
        $sex = check_input($_POST["sex"]);
    }

    if (empty($_POST["microchip"])) {
        $errorMessages = "Microchip is required";
    } else {
        $microchip = check_input($_POST["microchip"]);
    }

    if (empty($_POST["healthCard"])) {
        $errorMessages = "Health Card is required";
    } else {
        $healthCard = check_input($_POST["healthCard"]);
    }

    if (empty($_POST["owner"])) {
        $errorMessages = "Owner is required";
    } else {
        $owner = check_input($_POST["owner"]);
    }

    if (empty($_POST["id"])) {
        $errorMessages = "Something went wrong.. try again!";
    } else {
        $id = check_input($_POST['id']);
    }


    if (count($errorMessages) > 0) {
        foreach ($errorMessages as $error) {
            echo "
<div class='container'>
<div class=\"alert alert-danger\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  Hello, <b>$owner</b>. Seems to have some problems.. <b>$error</b> 
</div>
</div>";
        }
    } else {
        $id = $_GET['id'];
        $object = new Patient();
        $object->updatePatient($name, $species, $race, $birthDate, $sex, $microchip, $healthCard, $owner, $id);

        session_start();
        $_SESSION['message'] = "Patient $name was successfully edited";
        header('location: PatientsList.php');
    }
}

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}