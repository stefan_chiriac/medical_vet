<?php
require($_SERVER['DOCUMENT_ROOT'] . "/src/Patients/Service/Patient.php");
session_start();
$id = $_GET['id'];

$patient = new Patient();

$patient->deletePatient($id);
$_SESSION['message'] = "Patient deleted!";
header('location: PatientsList.php');
