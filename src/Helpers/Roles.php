<?php
session_start();


class Roles extends User {

    const USER = 'ROLE_USER';
    const DOCTOR = 'ROLE_DOCTOR';
    const ADMIN = 'ROLE_ADMIN';

    public function __construct() {
        parent::__construct();
    }


    function getRoleBySession()
    {
            $email = $_SESSION['email'];

            $query = $this->db->prepare("SELECT role FROM users WHERE email = :email");
            $query->bindParam("email",$email,PDO::PARAM_STR);
            $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
        }

        function returnRole($data)
        {
            foreach ($data as $user) {
                return $user['role'];
            }
        }

        function getAccessOnPageByRole($pageRole)
        {
            $userRole = $this->getRoleBySession();
            if($this->returnRole($userRole) == $pageRole)
            {
                return true;
            } return false;
        }

}