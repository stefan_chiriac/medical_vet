<!DOCTYPE>
<html lang="en">
<head>
    <title>Edit Patient Data</title>
    <?php include("../../../src/Layout/header.php"); ?>
    <link rel="stylesheet" href="../../../resources/css/main.css">
    <link rel="stylesheet" href="../../../resources/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resources/css/jquery-ui.min.css">
</head>

<body>
<?php include("../../../src/Users/Service/UserEdit.php");
include("../../../config/auth_session.php");
?>

<br/><br/>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div id='tablePanelBody' class='panel-body'>
                        <form name="update_user" method="post"
                              action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>?id=<?php echo $id ?>">
                            <table class='table table-hover' id="userEdit">
                                <?php foreach ($user as $users) : ?>
                                    <tr>
                                        <td>ID #<?php echo $users['id']; ?></td>

                                    </tr>
                                    <tr>
                                        <td>First Name</td>
                                        <td><input type="text" class="form-control" name="first_name"
                                                   value=<?php echo $users['first_name']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Last Name</td>
                                        <td><input type="text" class="form-control" name="last_name"
                                                   value=<?php echo $users['last_name']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><input type="email" class="form-control" name="email"
                                                   value=<?php echo $users['email']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td><input type="text" class="form-control" name="address"
                                                   value=<?php echo $users['address']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>City</td>
                                        <td><input type="text" class="form-control" name="city"
                                                   value=<?php echo $users['city']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td>Phone Number</td>
                                        <td><input type="tel" class="form-control" name="phone_number"
                                                   value=<?php echo $users['phone_number']; ?>></td>
                                    </tr>
                                    <tr>
                                        <td><input type="hidden" value="<?php echo $users['id']; ?>" name="id"></td>
                                        <td><input type="submit" name="submit" value="Submit" class="btn btn-primary">
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>