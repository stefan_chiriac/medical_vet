<!DOCTYPE>
<html lang="en">
<head>
    <title>Medical Vet - Register</title>
    <?php include("../../../src/Layout/header.php"); ?>
    <link rel="stylesheet" href="/resources/css/main.css">
    <link rel="stylesheet" href="/resources/css/daterangepicker.min.css">
</head>
<body>
<?php include("../../../src/Users/Service/UserRegistration.php"); ?>

<div class="container">
    <br>
    <p class="text-center">Registration Form</p>
    <hr>
</div>

<div class="border-form">
    <form class="main-form" name="register" method="post"
          action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
        <div class="form-group">
            <label for="first_name">First Name</label>
            <input type="text" placeholder="Doe" name="first_name" id="first_name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input type="text" placeholder="John" name="last_name" id="last_name" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="phone_number">Phone Number</label>
            <input type="tel" placeholder="07xx xxx xxx" name="phone_number" id="phone_number" class="form-control"
                   minlength="10" maxlength="10" pattern="[0-9]{3}[0-9]{3}[0-9]{4}"
                   required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" placeholder="john.doe@email.com" name="email" id="email" class="form-control" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" placeholder="****" name="password" id="password" class="form-control" minlength="8"
                   maxlength="100"
                   required>
        </div>
        <div class="form-group">
            <label for="passwordConfirmation">Password Confirmation</label>
            <input type="password" placeholder="****" name="passwordConfirmation" id="passwordConfirmation"
                   class="form-control" minlength="8"
                   maxlength="100"
                   required>
        </div>
        <div class="form-group">
            <input type="submit" name="submit" value="Submit" class="btn btn-primary">
        </div>
    </form>
</div>
</body>
</html>