<!DOCTYPE>
<html lang="en">
<head>
    <title>Medical Vet - Users List</title>

 </head>
<body>
<?php
include("../../../src/Users/Service/UserList.php");
include("../../../config/auth_session.php");
include("../../../src/Layout/Messages.php");
?>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/resources/css/all.css">
<script src="/resources/js/jquery-3.4.1.min.js"></script>
<link rel="stylesheet" href="/resources/css/datatables.css">
<script src="/resources/js/datatables.js"></script>
<link rel="stylesheet" href="/resources/css/bootstrap.css">
<link rel="stylesheet" href="/resources/css/main.css">
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<div class="container">
    <div class="table-wrapper">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-5">
                    <h2>Users <b>List</b></h2>
                </div>
                <div class="col-sm-7">
                    <a href="#" class="btn btn-primary"><i class="fa fa-user-plus"></i><span>Add New User</span></a>
                    <a href="#" class="btn btn-primary"><i class="fa fa-file-export"></i><span>Export to Excel</span></a>
                </div>
            </div>
        </div>
                        <table class="table table-striped" id="usersTable">
                            <thead>
                            <tr>
                                <th class="th" scope="col"> #</th>
                                <th scope="col"> First Name</th>
                                <th scope="col"> Last Name</th>
                                <th scope="col"> Role</th>
                                <th scope="col"> Status</th>
                                <th scope="col"> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($user as $users) : ?>
                                <tr>
                                    <th scope="row"><?php echo $users['id']; ?></th>
                                    <td><?php echo $users['first_name']; ?></td>
                                    <td><?php echo $users['last_name']; ?></td>
                                    <td><?php echo $users['role']; ?></td>
                                    <td><?php echo $users['status']; ?></td>
                                    <td>
                                        <a href="UserEdit.php?id=<?php echo $users['id'] ?>"><em class="fa fa-edit"
                                                                                                    title="Edit User"></em></a>
                                        <a href="UserView.php?id=<?php echo $users['id'] ?>"><em
                                                    class="fa fa-info-circle" title="View User"></em></a>
                                        <a href="UserDelete.php?id=<?php echo $users['id'] ?>"><em class="fa fa-trash"
                                                                                                      title="Delete User"></em></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
<script>
    $(document).ready(function () {
        $('#usersTable').dataTable({
            "searching": false,
            "order": [],
            "ordering": false,
        });
    });
</script>

</body>
</html>