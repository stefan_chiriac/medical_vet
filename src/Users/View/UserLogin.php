<!DOCTYPE>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Login</title>
    <link rel="stylesheet" href="/resources/css/main2.css"/>
</head>
<body>
<?php include("../../../src/Users/Service/UserLogin.php");
include("../../../src/Layout/header.php");?>

<form class="form" method="post" name="login">
    <h1 class="login-title">Login</h1>
    <input type="text" class="login-input" name="email" placeholder="email" autofocus="true"/>
    <input type="password" class="login-input" name="password" placeholder="Password"/>
    <input type="submit" value="Login" name="submit" class="login-button"/>
    <p class="link"><a href="/src/Users/View/UserRegister.php">New Registration</a></p>
</form>
</body>
</html>


