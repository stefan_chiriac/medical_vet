<!DOCTYPE>
<html lang="en">
<head>
    <title>Medical Vet - Patients List</title>
    <?php include("../../../src/Layout/header.php"); ?>
    <link rel="stylesheet" href="/resources/css/main.css">

</head>
<body>
<?php
include("../../../src/Users/Service/UserView.php");
include("../../../config/auth_session.php");
?>
<div class="container">
    <br>
    <p class="text-center">Users List</p>
    <hr>
</div>
<form class='main-form' name='view'>
    <?php foreach ($details as $view) : ?>
        <div class='form-group'>
            <label for='id'>Id</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['id']; ?>" readonly='true' id="id">
        </div>
        <div class='form-group'>
            <label for='first_name'>First Name</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['first_name']; ?>" readonly='true'
                   id="first_name">
        </div>
        <div class='form-group'>
            <label for='last_name'>Last Name</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['last_name']; ?>" readonly='true'
                   id="last_name">
        </div>
        <div class='form-group'>
            <label for='email'>Email</label>
            <input type='email' class='form-control' placeholder="<?php echo $view['email']; ?>" readonly='true'
                   id="email">
        </div>
        <div class='form-group'>
            <label for='address'>Address</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['address']; ?>" readonly='true'
                   id="address">
        </div>
        <div class='form-group'>
            <label for='city'>City</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['city']; ?>" readonly='true'
                   id="city">
        </div>
        <div class='form-group'>
            <label for='phone_number'>Phone Number</label>
            <input type='tel' class='form-control' placeholder="<?php echo $view['phone_number']; ?>" readonly='true'
                   id="phone_number">
        </div>
        <div class='form-group'>
            <label for='created'>Created</label>
            <input type='text' class='form-control' placeholder="<?php echo $view['created']; ?>" readonly='true'
                   id="created">
        </div>
    <?php endforeach; ?>

</form>

<br>
</body>
<?php include("../../../src/Layout/footer.php"); ?>
</html>