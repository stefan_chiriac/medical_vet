<?php

interface UserInterface {
    public function createUser($firstName, $lastName, $email, $password, $phoneNumber);
    public function getAllUsers();
    public function deleteUser($userId);
    public function updateUser($firstName, $lastName, $email, $address, $city, $phoneNumber, $id);
    public function getUserDetails($userId);
    public function logIn($email, $password);
    public function isLoggedIn();
    public function logOut();

}