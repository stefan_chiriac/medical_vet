<?php

require($_SERVER['DOCUMENT_ROOT'] . "/src/Users/Service/User.php");
session_start();
$id = $_GET['id'];

$user = new User();

$user->deleteUser($id);
$_SESSION['message'] = "User deleted!";
header('location: UserList.php');
