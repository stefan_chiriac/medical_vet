<?php

require($_SERVER['DOCUMENT_ROOT'] . "/config/config.php");
include($_SERVER['DOCUMENT_ROOT'] . "/src/Users/Interfaces/UserInterface.php");

class User implements UserInterface
{
    protected $db;

    public function __construct()
    {
        $this->db = Config();
    }

    public function __destruct()
    {
        $this->db = null;
    }

    public function createUser($firstName, $lastName, $email, $password, $phoneNumber)
    {
        $query = $this->db->prepare("INSERT INTO users (first_name, last_name, email, password, phone_number) VALUES (:firstName, :lastName, :email, :password, :phoneNumber)");
        $query->bindParam("firstName", $firstName, PDO::PARAM_STR);
        $query->bindParam("lastName", $lastName, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam("password", $password, PDO::PARAM_STR);
        $query->bindParam("phoneNumber", $phoneNumber, PDO::PARAM_STR);
        $query->execute();
        return $this->db->lastInsertId();
    }

    public function getAllUsers()
    {
        $query = $this->db->prepare("SELECT * FROM users");
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function deleteUser($userId)
    {
        $query = $this->db->prepare("DELETE FROM users WHERE id = :id");
        $query->bindParam("id", $userId, PDO::PARAM_STR);
        $query->execute();
    }

    public function updateUser($firstName, $lastName, $email, $address, $city, $phoneNumber, $id)
    {
        $query = $this->db->prepare("UPDATE users SET first_name = :firstName, last_name = :lastName, email = :email, address = :address, city = :city, phone_number = :phoneNumber WHERE id = :id");
        $query->bindParam("id", $id, PDO::PARAM_STR);
        $query->bindParam("firstName", $firstName, PDO::PARAM_STR);
        $query->bindParam("lastName", $lastName, PDO::PARAM_STR);
        $query->bindParam("email", $email, PDO::PARAM_STR);
        $query->bindParam("address", $address, PDO::PARAM_STR);
        $query->bindParam("city", $city, PDO::PARAM_STR);
        $query->bindParam("phoneNumber", $phoneNumber, PDO::PARAM_STR);
        $query->execute();
    }

    public function getUserDetails($userId)
    {
        $query = $this->db->prepare("SELECT * FROM users WHERE id = :id");
        $query->bindParam("id", $userId, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }

    public function logIn($email, $password)
    {
        $db = Config();
        $query = $db->prepare("SELECT email,password FROM users WHERE email = :email AND password = :password");
        $query->bindParam('email', $email, PDO::PARAM_STR);
        $hashPassword = hash('sha256', $password);
        $query->bindParam('password', $hashPassword, PDO::PARAM_STR);
        $query->execute();
        if ($query->rowCount() > 0) {
            $result = $query->fetch(PDO::FETCH_OBJ);
            return true;
        }
    }

    public function isLoggedIn()
    {
        session_start();
        if (isset($_SESSION['email'])) {
            $_SESSION['message'] = 'You are already logged in!';
            header("Location: http://".$_SERVER['HTTP_HOST'].'/index.php');
        }
    }

    public function logOut()
    {
        session_start();
        if (isset($_SESSION['email'])) {
            unset($_SESSION['email']);
            $_SESSION['message'] = 'You have been successfully logged out!';
            header("Location: http://".$_SERVER['HTTP_HOST'].'/index.php');
        } else {
            $_SESSION['message'] = 'You are not logged in!';
            header("Location: http://".$_SERVER['HTTP_HOST'].'/index.php');
        }
    }

    public function getUserIdByEmail($userEmail)
    {
        $query = $this->db->prepare("SELECT id FROM users where email = :email");
        $query->bindParam("email", $userEmail, PDO::PARAM_STR);
        $query->execute();
        $data = array();
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $row;
        }
        return $data;
    }
}
