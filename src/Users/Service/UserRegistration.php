<?php

require($_SERVER['DOCUMENT_ROOT'] . "/src/Users/Service/User.php");

$errorMessages = [];

$firstName = $lastName = $password = $confirmPassword = $email = $phoneNumber = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["first_name"])) {
        $errorMessages[] = "Name is required";
    } else {
        $firstName = check_input($_POST["first_name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/", $firstName)) {
            $errorMessages[] = "Space and special characters not allowed but you can use underscore(_).";
        } else {
            $firstName = check_input($_POST['first_name']);
        }
    }

    if (empty($_POST["last_name"])) {
        $errorMessages[] = "Name is required";
    } else {
        $lastName = check_input($_POST["last_name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/", $lastName)) {
            $errorMessages[] = "Space and special characters not allowed but you can use underscore(_).";
        } else {
            $lastName = check_input($_POST['last_name']);
        }
    }

    if (empty($_POST["email"])) {
        $errorMessages = "Email is required";
    } else {
        $email = check_input($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errorMessages = "Invalid email format";
        } else {
            $email = check_input($_POST['email']);
        }
    }

    if (empty($_POST["password"])) {
        $errorMessages = "Password is required";
    } else {
        $password = check_input($_POST["password"]);
    }

    if (empty($_POST["passwordConfirmation"])) {
        $errorMessages = "Confirm your Password!";
    } else {
        $confirmPassword = check_input($_POST["passwordConfirmation"]);
        //Confirm if password match
        if ($confirmPassword != $password) {
            $errorMessages = "Password not match!";
        }
    }

    if (empty($_POST["phone_number"])) {
        $errorMessages = "Phone Number is required!";
    } else {
        $phoneNumber = check_input($_POST["phone_number"]);
    }

    $hashPassword = hash('sha256', $password);


    if (count($errorMessages) > 0) {
        foreach ($errorMessages as $error) {
            echo "
<div class='container'>
<div class=\"alert alert-danger\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  Hello, <b>$firstName</b>. Seems to have some problems.. <b>$error</b> 
</div>
</div>";
        }
    } else {
        $object = new User();

        $object->createUser($firstName, $lastName, $email, $hashPassword, $phoneNumber);

        echo "<div class='container'>
<div class='alert alert-success' role='alert'>
<div class='fa fa-check-square fa-fw'></div> Welcome, <b>$firstName</b>. Your account was successfully created.
</div>
</div>";
    }
}

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
