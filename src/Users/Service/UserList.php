<?php
require($_SERVER['DOCUMENT_ROOT'] . "/src/Users/Service/User.php");
require($_SERVER['DOCUMENT_ROOT'] . "/src/Helpers/Roles.php");

session_start();

$role = new Roles();
$object = new User();

$pageRequiredRole = Roles::ADMIN;

if ($role->getAccessOnPageByRole($pageRequiredRole)) {
    $user = $object->getAllUsers();
} else {
    $_SESSION['warning'] = 'Access Denied!';
    header("Location: http://" . $_SERVER['HTTP_HOST'] . '/index.php');
    exit;
}


