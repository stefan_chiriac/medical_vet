<?php require($_SERVER['DOCUMENT_ROOT'] . "/src/Users/Service/User.php");

$object = new User();

session_start();
if (isset($_SESSION['email'])) {
    $_SESSION['message'] = 'You are already logged in.';
    header("Location: http://" . $_SERVER['HTTP_HOST'] . '/index.php');
} else {
    if (isset($_POST['email'])) {
        $email = stripslashes($_REQUEST['email']);
        $password = stripslashes($_REQUEST['password']);
        if ($object->logIn($email, $password)) {
            $_SESSION['email'] = $email;
            $_SESSION['success'] = 'You have been successfully logged in!';
            header("Location: http://" . $_SERVER['HTTP_HOST'] . '/index.php');
        } else {
            echo "<div class='container'>
<div class=\"alert alert-info\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  <b>Invalid email/password.</b> Try again
</div>
</div>";
        }
    }
}