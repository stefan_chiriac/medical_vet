<?php

require($_SERVER['DOCUMENT_ROOT'] . "/src/Users/Service/User.php");

$errorMessages = [];
$object = new User();

$id = $_GET['id'];

$user = $object->getUserDetails($id);

$firstName = $lastName = $password = $confirmPassword = $email = $phoneNumber = $address = $city = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["first_name"])) {
        $errorMessages[] = "Name is required";
    } else {
        $firstName = check_input($_POST["first_name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/", $firstName)) {
            $errorMessages[] = "Space and special characters not allowed but you can use underscore(_).";
        } else {
            $firstName = check_input($_POST['first_name']);
        }
    }

    if (empty($_POST["last_name"])) {
        $errorMessages[] = "Name is required";
    } else {
        $lastName = check_input($_POST["last_name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z0-9_]*$/", $lastName)) {
            $errorMessages[] = "Space and special characters not allowed but you can use underscore(_).";
        } else {
            $lastName = check_input($_POST['last_name']);
        }
    }

    if (empty($_POST["email"])) {
        $errorMessages[] = "Email is required";
    } else {
        $email = check_input($_POST["email"]);
        // check if e-mail address is well-formed
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errorMessages[] = "Invalid email format";
        } else {
            $email = check_input($_POST['email']);
        }
    }

    if (empty($_POST["phone_number"])) {
        $errorMessages[] = "Phone Number is required!";
    } else {
        $phoneNumber = check_input($_POST["phone_number"]);
    }

    if (empty($_POST["address"])) {
        $errorMessages[] = "Address is required!";
    } else {
        $address = check_input($_POST["address"]);
    }

    if (empty($_POST["city"])) {
        $errorMessages[] = "Address is required!";
    } else {
        $city = check_input($_POST["city"]);
    }

    if (empty($_POST["id"])) {
        $errorMessages[] = "Something went wrong.. try again!";
    } else {
        $id = check_input($_POST['id']);
    }

    if (count($errorMessages) > 0) {
        foreach ($errorMessages as $error) {
            echo "
<div class='container'>
<div class=\"alert alert-danger\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  Hello, <b>$firstName</b>. Seems to have some problems.. <b>$error</b> 
</div>
</div>";
        }
    } else {
        $id = $_GET['id'];
        $object = new User();
        $object->updateUser($firstName, $lastName, $email, $address, $city, $phoneNumber, $id);
        session_start();
        $_SESSION['message'] = "User $firstName was successfully edited";
        header('location: UserList.php');
    }
}

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
