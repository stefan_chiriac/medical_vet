<?php
session_start();

if (isset($_SESSION['message'])) {
    $message = $_SESSION['message'];
    echo "<div class='container'>
<div class=\"alert alert-info\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  <b>$message</b>
</div>
</div>";
    unset($_SESSION['message']);
} elseif (isset($_SESSION['warning'])) {
    $message = $_SESSION['warning'];
    echo "<div class='container'>
<div class=\"alert alert-danger\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  <b>$message</b>
</div>
</div>";
    unset($_SESSION['warning']);
} elseif (isset($_SESSION['success'])) {
    $message = $_SESSION['success'];
    echo "<div class='container'>
<div class=\"alert alert-success\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  <b>$message</b>
</div>
</div>";
    unset($_SESSION['success']);
} elseif (isset($_SESSION['error'])) {
    $message = $_SESSION['error'];
    echo "<div class='container'>
<div class=\"alert alert-danger\" role=\"alert\">
  <div class='fa fa-exclamation-triangle fa-fw'>  </div>  <b>$message</b>
</div>
</div>";
    unset($_SESSION['error']);
}